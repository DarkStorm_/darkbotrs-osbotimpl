import java.util.logging.*;


public final class SLFLoggerProxy extends Handler {
	private final org.slf4j.Logger slfLogger;
	private final Logger logger;

	public SLFLoggerProxy(org.slf4j.Logger slfLogger, Logger logger) {
		this.slfLogger = slfLogger;
		this.logger = logger;
	}

	@Override
	public void publish(LogRecord record) {
		record.setSourceClassName(record.getLoggerName());
		if(!logger.isLoggable(record.getLevel()))
			return;
		String message = record.getMessage();
		Throwable thrown = record.getThrown();
		if(thrown != null) {
			if(record.getLevel() == Level.INFO)
				slfLogger.info(message, thrown);
			else if(record.getLevel() == Level.SEVERE)
				slfLogger.error(message, thrown);
			else if(record.getLevel() == Level.CONFIG)
				slfLogger.debug(message, thrown);
			else if(record.getLevel() == Level.FINE
					|| record.getLevel() == Level.FINER
					|| record.getLevel() == Level.FINEST)
				slfLogger.trace(message, thrown);
		} else if(message != null) {
			if(record.getLevel() == Level.INFO)
				slfLogger.info(message);
			else if(record.getLevel() == Level.SEVERE)
				slfLogger.error(message);
			else if(record.getLevel() == Level.CONFIG)
				slfLogger.debug(message);
			else if(record.getLevel() == Level.FINE
					|| record.getLevel() == Level.FINER
					|| record.getLevel() == Level.FINEST)
				slfLogger.trace(message);
		}
	}

	@Override
	public void flush() {
	}

	@Override
	public void close() throws SecurityException {
	}
}