package org.darkstorm.runescape.osbot;

import java.util.*;

import org.darkstorm.runescape.api.GroundItems;
import org.darkstorm.runescape.api.util.*;
import org.darkstorm.runescape.api.wrapper.GroundItem;
import org.darkstorm.runescape.osbot.wrapper.GroundItemWrapper;
import org.osbot.accessor.*;
import org.osbot.script.rs2.utility.NodeDequeIterator;

public final class GroundItemsImpl extends AbstractUtility implements
		GroundItems {

	public GroundItemsImpl(GameContextImpl context) {
		super(context);
	}

	@Override
	public GroundItem getClosest(Filter<GroundItem> filter) {
		GroundItem closest = null;
		double minDist = 0;
		Tile self = context.getPlayers().getSelf().getLocation();
		for(GroundItem groundItem : getAll()) {
			if(filter.accept(groundItem)) {
				double dist = self.distanceTo(groundItem.getLocation());
				if(dist < minDist || closest == null) {
					closest = groundItem;
					minDist = dist;
				}
			}
		}
		return closest;
	}

	@Override
	public GroundItem[] getAll(Filter<GroundItem> filter) {
		List<GroundItem> accepted = new ArrayList<GroundItem>();
		for(GroundItem groundItem : getAll())
			if(filter.accept(groundItem))
				accepted.add(groundItem);
		return accepted.toArray(new GroundItem[accepted.size()]);
	}

	@Override
	public GroundItem[] getAll() {
		List<GroundItem> items = new ArrayList<GroundItem>();
		NodeDequeIterator iterator = new NodeDequeIterator();
		XNodeDeque[][][] groundItems = client.getGroundItemDeques();
		for(int i = 0; i < groundItems.length; i++) {
			XNodeDeque[][] depthGroundItems = groundItems[i];
			for(int x = 0; x < depthGroundItems.length; x++) {
				XNodeDeque[] xGroundItems = depthGroundItems[x];
				for(int y = 0; y < xGroundItems.length; y++) {
					XNodeDeque item = xGroundItems[y];
					if(item == null)
						continue;
					iterator.set(item);
					XNode node;
					while((node = iterator.getNext()) != null) {
						if(!(node instanceof XGroundItem))
							continue;
						GroundItem groundItem = new GroundItemWrapper(context,
								(XGroundItem) node, client.getMapBaseX() + x,
								client.getMapBaseY() + y);
						items.add(groundItem);
					}
				}
			}
		}
		return items.toArray(new GroundItem[items.size()]);
	}
}
