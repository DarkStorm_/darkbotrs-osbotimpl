package org.darkstorm.runescape.osbot;

import java.applet.Applet;
import java.awt.*;
import java.awt.event.*;
import java.io.IOException;
import java.lang.reflect.*;
import java.lang.reflect.Method;
import java.net.*;
import java.util.*;
import java.util.List;
import java.util.jar.*;
import java.util.logging.*;

import org.darkstorm.runescape.*;
import org.darkstorm.runescape.event.*;
import org.darkstorm.runescape.script.*;
import org.osbot.engine.BotCanvas;
import org.osbot.engine.input.BotEvent;
import org.osbot.script.Script;
import org.osbot.script.rs2.Client;
import org.slf4j.LoggerFactory;

import com.sun.org.apache.bcel.internal.classfile.*;
import com.sun.org.apache.bcel.internal.generic.*;

public class OSBot implements Bot {
	private final DarkBot darkbot;
	private final org.osbot.engine.Bot bot;
	private final Logger logger;
	private final EventManager eventManager;
	private final GameContextImpl context;
	private final ScriptManager scriptManager;
	private final RandomEventManager randomEventManager;

	private final Map<Integer, Constructor<? extends MouseEvent>> mouseEvents;
	private final Map<Integer, Constructor<? extends KeyEvent>> keyEvents;
	private final Method setHumanInputMethod, getScriptMethod, getClientMethod,
			getCanvasMethod;

	public OSBot(DarkBot darkbot, org.osbot.engine.Bot bot) {
		this.darkbot = darkbot;
		this.bot = bot;

		mouseEvents = new HashMap<>();
		keyEvents = new HashMap<>();
		try {
			List<Class<?>> classes = getClassesInPackage(
					org.osbot.engine.Bot.class.getClassLoader(), "org.osbot");
			List<Class<? extends MouseEvent>> mouseEvents = new ArrayList<>();
			List<Class<? extends KeyEvent>> keyEvents = new ArrayList<>();
			for(Class<?> c : classes) {
				Class<?> superclass = c.getSuperclass();
				if(superclass == null)
					continue;
				if(!BotEvent.class.isAssignableFrom(superclass)
						|| Modifier.isAbstract(c.getModifiers()))
					continue;
				if(MouseEvent.class.isAssignableFrom(superclass))
					mouseEvents.add(c.asSubclass(MouseEvent.class));
				if(KeyEvent.class.isAssignableFrom(superclass))
					keyEvents.add(c.asSubclass(KeyEvent.class));
			}
			List<ClassGen> classGens = loadClassGens(
					Bot.class.getClassLoader(), "org.osbot");
			for(ClassGen classGen : classGens) {
				for(Class<? extends MouseEvent> mouseEvent : mouseEvents) {
					if(!mouseEvent.getName().equals(classGen.getClassName()))
						continue;
					for(com.sun.org.apache.bcel.internal.classfile.Method method : classGen
							.getMethods()) {
						if(!method.getName().equals("pushEvent"))
							continue;
						MethodGen methodGen = new MethodGen(method,
								classGen.getClassName(),
								classGen.getConstantPool());
						InstructionList instructionList = methodGen
								.getInstructionList();
						Instruction[] instructions = instructionList
								.getInstructions();
						for(int i = instructions.length - 1; i >= 0; i--) {
							Instruction ins = instructions[i];
							if(!(ins instanceof INVOKEINTERFACE))
								continue;
							String methodName = ((INVOKEINTERFACE) ins)
									.getMethodName(classGen.getConstantPool());
							int eventType;
							switch(methodName) {
							case "mouseMoved":
								eventType = MouseEvent.MOUSE_MOVED;
								break;
							case "mouseClicked":
								eventType = MouseEvent.MOUSE_CLICKED;
								break;
							case "mousePressed":
								eventType = MouseEvent.MOUSE_PRESSED;
								break;
							case "mouseReleased":
								eventType = MouseEvent.MOUSE_RELEASED;
								break;
							case "mouseDragged":
								eventType = MouseEvent.MOUSE_DRAGGED;
								break;
							case "mouseEntered":
								eventType = MouseEvent.MOUSE_ENTERED;
								break;
							case "mouseExited":
								eventType = MouseEvent.MOUSE_EXITED;
								break;
							default:
								continue;
							}
							Constructor<? extends MouseEvent> mouseEventConstructor = null;
							for(Constructor<?> constructor : mouseEvent
									.getConstructors())
								if(constructor.getParameterTypes()[0]
										.equals(Component.class))
									mouseEventConstructor = mouseEvent
											.getConstructor(constructor
													.getParameterTypes());
							if(mouseEventConstructor != null) {
								System.out.println("Event found: " + methodName
										+ " = " + mouseEvent.getName());
								this.mouseEvents.put(eventType,
										mouseEventConstructor);
							}
							break;
						}
					}
				}
				for(Class<? extends KeyEvent> keyEvent : keyEvents) {
					if(!keyEvent.getName().equals(classGen.getClassName()))
						continue;
					for(com.sun.org.apache.bcel.internal.classfile.Method method : classGen
							.getMethods()) {
						if(!method.getName().equals("pushEvent"))
							continue;
						MethodGen methodGen = new MethodGen(method,
								classGen.getClassName(),
								classGen.getConstantPool());
						InstructionList instructionList = methodGen
								.getInstructionList();
						Instruction[] instructions = instructionList
								.getInstructions();
						for(int i = instructions.length - 1; i >= 0; i--) {
							Instruction ins = instructions[i];
							if(!(ins instanceof INVOKEINTERFACE))
								continue;
							String methodName = ((INVOKEINTERFACE) ins)
									.getMethodName(classGen.getConstantPool());
							int eventType;
							switch(methodName) {
							case "keyPressed":
								eventType = KeyEvent.KEY_PRESSED;
								break;
							case "keyReleased":
								eventType = KeyEvent.KEY_RELEASED;
								break;
							case "keyTyped":
								eventType = KeyEvent.KEY_TYPED;
								break;
							default:
								continue;
							}
							Constructor<? extends KeyEvent> keyEventConstructor = null;
							for(Constructor<?> constructor : keyEvent
									.getConstructors())
								if(constructor.getParameterTypes()[0]
										.equals(Component.class))
									keyEventConstructor = keyEvent
											.getConstructor(constructor
													.getParameterTypes());
							if(keyEventConstructor != null) {
								System.out.println("Key event found: "
										+ methodName + " = "
										+ keyEvent.getName());
								this.keyEvents.put(eventType,
										keyEventConstructor);
							}
							break;
						}
					}
				}
			}
		} catch(Exception e) {
			e.printStackTrace();
		}

		Method setHumanInputMethod = null, getScriptMethod = null, getCanvasMethod = null, getClientMethod = null;
		for(Method method : bot.getClass().getMethods()) {
			if(!Modifier.isPublic(method.getModifiers())
					|| Modifier.isStatic(method.getModifiers()))
				continue;
			if(method.getParameterTypes().length == 1
					&& method.getParameterTypes()[0] == Boolean.TYPE
					&& method.getReturnType() == Void.class)
				setHumanInputMethod = method;
			else if(method.getParameterTypes().length == 0)
				if(method.getReturnType() == BotCanvas.class)
					getCanvasMethod = method;
				else if(method.getReturnType() == Client.class)
					getClientMethod = method;
				else if(method.getReturnType() == Script.class)
					getScriptMethod = method;
		}
		System.out.println("setHumanInput: " + setHumanInputMethod);
		System.out.println("getScript: " + getScriptMethod);
		System.out.println("getCanvas: " + getCanvasMethod);
		System.out.println("getClient: " + getClientMethod);
		this.setHumanInputMethod = setHumanInputMethod;
		this.getScriptMethod = getScriptMethod;
		this.getCanvasMethod = getCanvasMethod;
		this.getClientMethod = getClientMethod;

		final org.slf4j.Logger botLogger = LoggerFactory.getLogger(getName());

		logger = Logger.getLogger(getName());
		for(Handler handler : logger.getHandlers())
			logger.removeHandler(handler);
		logger.addHandler(new Handler() {
			@Override
			public void publish(LogRecord record) {
				record.setSourceClassName(logger.getName());
				if(!logger.isLoggable(record.getLevel()))
					return;
				String message = record.getMessage();
				Throwable thrown = record.getThrown();
				if(thrown != null) {
					if(record.getLevel() == Level.INFO)
						botLogger.info(message, thrown);
					else if(record.getLevel() == Level.SEVERE)
						botLogger.error(message, thrown);
					else if(record.getLevel() == Level.CONFIG)
						botLogger.debug(message, thrown);
					else if(record.getLevel() == Level.FINE
							|| record.getLevel() == Level.FINER
							|| record.getLevel() == Level.FINEST)
						botLogger.trace(message, thrown);
				} else if(message != null) {
					if(record.getLevel() == Level.INFO)
						botLogger.info(message);
					else if(record.getLevel() == Level.SEVERE)
						botLogger.error(message);
					else if(record.getLevel() == Level.CONFIG)
						botLogger.debug(message);
					else if(record.getLevel() == Level.FINE
							|| record.getLevel() == Level.FINER
							|| record.getLevel() == Level.FINEST)
						botLogger.trace(message);
				}
			}

			@Override
			public void flush() {
			}

			@Override
			public void close() throws SecurityException {
			}
		});
		eventManager = new BasicEventManager();
		context = new GameContextImpl(this);
		scriptManager = new ScriptManagerImpl(this);
		randomEventManager = new RandomEventManagerImpl(this);
	}

	@Override
	public String getName() {
		return "NotOSBot";
	}

	@Override
	public InputState getInputState() {
		return bot.isHumanInputToggled() ? InputState.MOUSE_KEYBOARD
				: InputState.NONE;
	}

	@Override
	public void setInputState(InputState state) {
		boolean toggled;
		switch(state) {
		case MOUSE_KEYBOARD:
			toggled = true;
			break;
		case KEYBOARD:
			toggled = !bot.isHumanInputToggled();
			break;
		default:
			toggled = false;
		}
		if(setHumanInputMethod != null) {
			try {
				setHumanInputMethod.invoke(bot, toggled);
			} catch(Exception e) {
				e.printStackTrace();
			}
		}
	}

	@Override
	public boolean canPlayScript() {
		Script script = null;
		if(getScriptMethod != null) {
			try {
				script = (Script) getScriptMethod.invoke(bot);
			} catch(Exception e) {
				e.printStackTrace();
			}
		}
		return bot.isActive() && script == null;
	}

	@Override
	public Component getDisplay() {
		Client client = null;
		if(getClientMethod != null) {
			try {
				client = (Client) getClientMethod.invoke(bot);
			} catch(Exception e) {
				e.printStackTrace();
			}
		}
		if(client == null)
			return null;
		return ((Applet) client.instance).getParent();
	}

	@Override
	public Applet getGame() {
		Client client = null;
		if(getClientMethod != null) {
			try {
				client = (Client) getClientMethod.invoke(bot);
			} catch(Exception e) {
				e.printStackTrace();
			}
		}
		if(client == null)
			return null;
		return (Applet) client.instance;
	}

	@Override
	public Canvas getCanvas() {
		BotCanvas canvas = null;
		if(getCanvasMethod != null) {
			try {
				canvas = (BotCanvas) getCanvasMethod.invoke(bot);
			} catch(Exception e) {
				e.printStackTrace();
			}
		}
		return canvas;
	}

	public Client getClient() {
		Client client = null;
		if(getClientMethod != null) {
			try {
				client = (Client) getClientMethod.invoke(bot);
			} catch(Exception e) {
				e.printStackTrace();
			}
		}
		return client;
	}

	@Override
	public Logger getLogger() {
		return logger;
	}

	@Override
	public void dispatchInputEvent(InputEvent event) {
		if(event instanceof MouseEvent)
			// event = new BotMouseEvent(event.getComponent(), event.getID(),
			// event.getWhen(), event.getModifiers(),
			// ((MouseEvent) event).getX(), ((MouseEvent) event).getY(),
			// ((MouseEvent) event).getClickCount(),
			// ((MouseEvent) event).isPopupTrigger(),
			// ((MouseEvent) event).getButton());
			event = wrapMouseEvent((MouseEvent) event);
		else if(event instanceof KeyEvent)
			// event = new BotKeyEvent(event.getComponent(), event.getID(),
			// event.getWhen(), event.getModifiers(),
			// ((KeyEvent) event).getKeyCode(),
			// ((KeyEvent) event).getKeyChar(),
			// ((KeyEvent) event).getKeyLocation());
			event = wrapKeyEvent((KeyEvent) event);
		else
			return;
		if(event instanceof BotEvent) {
			try {
				((BotEvent) event).pushEvent(bot);
			} catch(InterruptedException e) {
				e.printStackTrace();
			}
			return;
		}
		Client client = null;
		if(getClientMethod != null) {
			try {
				client = (Client) getClientMethod.invoke(bot);
			} catch(Exception e) {
				e.printStackTrace();
			}
		}
		if(client == null)
			return;
		MouseListener mouseListener = client.getMouseListener();
		MouseMotionListener mouseMotionListener = client
				.getMouseMotionListener();
		KeyListener keyListener = client.getKeyListener();
		if(event instanceof MouseEvent) {
			MouseEvent mouseEvent = (MouseEvent) event;
			switch(mouseEvent.getID()) {
			case MouseEvent.MOUSE_PRESSED:
				mouseListener.mousePressed(mouseEvent);
				break;
			case MouseEvent.MOUSE_RELEASED:
				mouseListener.mouseReleased(mouseEvent);
				break;
			case MouseEvent.MOUSE_CLICKED:
				mouseListener.mouseClicked(mouseEvent);
				break;
			case MouseEvent.MOUSE_ENTERED:
				mouseListener.mouseEntered(mouseEvent);
				break;
			case MouseEvent.MOUSE_EXITED:
				mouseListener.mouseExited(mouseEvent);
				break;
			case MouseEvent.MOUSE_MOVED:
				mouseMotionListener.mouseMoved(mouseEvent);
				break;
			case MouseEvent.MOUSE_DRAGGED:
				mouseMotionListener.mouseDragged(mouseEvent);
				break;
			}
		} else if(event instanceof KeyEvent) {
			KeyEvent keyEvent = (KeyEvent) event;
			switch(keyEvent.getID()) {
			case KeyEvent.KEY_PRESSED:
				keyListener.keyPressed(keyEvent);
				break;
			case KeyEvent.KEY_RELEASED:
				keyListener.keyReleased(keyEvent);
				break;
			case KeyEvent.KEY_TYPED:
				keyListener.keyTyped(keyEvent);
				break;
			}
		}
		/*Applet game = (Applet) bot.getClient().instance;
		if(game == null)
			return;
		try {
			Method method = Component.class.getDeclaredMethod("processEvent",
					AWTEvent.class);
			method.setAccessible(true);
			method.invoke(game, event);
			fireEventRecursively(game, method, event);
		} catch(Exception exception) {
			exception.printStackTrace();
		}*/
	}

	private MouseEvent wrapMouseEvent(MouseEvent event) {
		Constructor<? extends MouseEvent> mouseEventConstructor = mouseEvents
				.get(event.getID());
		if(mouseEventConstructor == null)
			return event;
		try {
			return mouseEventConstructor.newInstance(event.getComponent(),
					event.getWhen(), event.getModifiers(), event.getX(),
					event.getY(), event.getClickCount(),
					event.isPopupTrigger(), event.getButton());
		} catch(Exception e) {
			e.printStackTrace();
			return event;
		}
	}

	private KeyEvent wrapKeyEvent(KeyEvent event) {
		Constructor<? extends KeyEvent> keyEventConstructor = keyEvents
				.get(event.getID());
		if(keyEventConstructor == null)
			return event;
		try {
			return keyEventConstructor.newInstance(event.getComponent(),
					event.getWhen(), event.getModifiers(), event.getKeyCode(),
					event.getKeyChar(), event.getKeyLocation());
		} catch(Exception e) {
			e.printStackTrace();
			return event;
		}
	}

	/*private void fireEventRecursively(Container container, Method method,
			AWTEvent e) throws Exception {
		for(Component component : container.getComponents()) {
			method.invoke(component, e);
			if(component instanceof Container)
				fireEventRecursively((Container) component, method, e);
		}
	}*/

	@SuppressWarnings("resource")
	private List<ClassGen> loadClassGens(ClassLoader classLoader,
			String packageName) throws IOException {
		List<ClassGen> classes = new ArrayList<>();
		URL packageURL;

		packageURL = classLoader.getResource(packageName.replace(".", "/"));

		if(packageURL.getProtocol().equals("jar")) {
			packageName = packageName.replace(".", "/");
			String jarFileName;
			JarFile jf;
			Enumeration<JarEntry> jarEntries;
			String entryName;

			// build jar file name, then loop through zipped entries
			jarFileName = URLDecoder.decode(packageURL.getFile(), "UTF-8");
			jarFileName = jarFileName.substring(5, jarFileName.indexOf("!"));
			jf = new JarFile(jarFileName);
			jarEntries = jf.entries();
			while(jarEntries.hasMoreElements()) {
				JarEntry entry = jarEntries.nextElement();
				entryName = entry.getName();
				if(entryName.startsWith(packageName)
						&& entryName.endsWith(".class")
						&& entryName.length() > packageName.length() + 5) {
					entryName = entryName.replace('/', '.');
					entryName = entryName.substring(0,
							entryName.lastIndexOf('.'));
					if(entryName.lastIndexOf('.') != packageName.length())
						continue;
					try {
						ClassParser parser = new ClassParser(
								jf.getInputStream(entry), "unknown");
						JavaClass jc = parser.parse();
						classes.add(new ClassGen(jc));
					} catch(Throwable exception) {
						System.out.println("Error: " + entryName);
					}
				}
			}

			// loop through files in classpath
		}
		return classes;
	}

	// Why do I have to do these kinds of things? ...
	/**
	 * @author Paulius Matulionis - stackoverflow.com
	 */
	@SuppressWarnings("resource")
	private List<Class<?>> getClassesInPackage(ClassLoader classLoader,
			String packageName) throws IOException {
		List<Class<?>> classes = new ArrayList<Class<?>>();
		URL packageURL;

		packageURL = classLoader.getResource(packageName.replace(".", "/"));

		if(packageURL.getProtocol().equals("jar")) {
			packageName = packageName.replace(".", "/");
			String jarFileName;
			JarFile jf;
			Enumeration<JarEntry> jarEntries;
			String entryName;

			// build jar file name, then loop through zipped entries
			jarFileName = URLDecoder.decode(packageURL.getFile(), "UTF-8");
			jarFileName = jarFileName.substring(5, jarFileName.indexOf("!"));
			jf = new JarFile(jarFileName);
			jarEntries = jf.entries();
			while(jarEntries.hasMoreElements()) {
				entryName = jarEntries.nextElement().getName();
				if(entryName.startsWith(packageName)
						&& entryName.endsWith(".class")
						&& entryName.length() > packageName.length() + 5) {
					entryName = entryName.replace('/', '.');
					entryName = entryName.substring(0,
							entryName.lastIndexOf('.'));
					if(entryName.lastIndexOf('.') != packageName.length())
						continue;
					try {
						classes.add(Class.forName(entryName));
					} catch(Throwable exception) {
						System.out.println("Error: " + entryName);
					}
				}
			}

			// loop through files in classpath
		}
		return classes;
	}

	@Override
	public EventManager getEventManager() {
		return eventManager;
	}

	@Override
	public ScriptManager getScriptManager() {
		return scriptManager;
	}

	@Override
	public RandomEventManager getRandomEventManager() {
		return randomEventManager;
	}

	@Override
	public GameContextImpl getGameContext() {
		return context;
	}

	public org.osbot.engine.Bot getBot() {
		return bot;
	}

	@Override
	public DarkBot getDarkBot() {
		return darkbot;
	}

}
