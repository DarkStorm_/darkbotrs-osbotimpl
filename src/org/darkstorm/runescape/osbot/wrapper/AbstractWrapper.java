package org.darkstorm.runescape.osbot.wrapper;

import org.darkstorm.runescape.api.GameContext;
import org.darkstorm.runescape.api.wrapper.Wrapper;
import org.darkstorm.runescape.osbot.GameContextImpl;

abstract class AbstractWrapper implements Wrapper {
	protected final GameContextImpl context;

	public AbstractWrapper(GameContextImpl context) {
		this.context = context;
	}

	@Override
	public GameContext getContext() {
		return context;
	}
}
