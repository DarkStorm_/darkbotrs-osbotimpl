package org.darkstorm.runescape.osbot.wrapper;

import java.awt.Point;

import org.darkstorm.runescape.api.input.*;
import org.darkstorm.runescape.api.util.Tile;
import org.darkstorm.runescape.api.wrapper.*;
import org.darkstorm.runescape.osbot.GameContextImpl;
import org.osbot.accessor.XAnimable;

public abstract class AnimableWrapper extends AbstractWrapper implements
		Animable {
	private XAnimable<?> handle;
	private ModelWrapper model;

	public AnimableWrapper(GameContextImpl context, XAnimable<?> handle) {
		super(context);
		this.handle = handle;
		org.osbot.script.rs2.model.Animable<?> animable = org.osbot.script.Wrapper
				.wrap(handle);
		model = new ModelWrapper(context, animable.model, this);
	}

	@Override
	public int getHeight() {
		return model.getModel().getHeight();
	}

	@Override
	public abstract Tile getLocation();

	@Override
	public Point getScreenLocation() {
		Tile location = getLocation();
		int height = getHeight();
		return context.getCalculations().getWorldScreenLocation(
				location.getPreciseX(), location.getPreciseY(),
				height / 2 + height / 4);
	}

	@Override
	public MouseTarget getTarget() {
		return new ModelMouseTarget(model);
	}

	@Override
	public boolean isOnScreen() {
		Point point = getTarget().getLocation();
		return point != null && context.getCalculations().isInGameArea(point);
	}

	@Override
	public Model getModel() {
		return model;
	}

	@Override
	public boolean equals(Object obj) {
		return obj instanceof AnimableWrapper && getClass() == obj.getClass()
				&& handle == ((AnimableWrapper) obj).handle;
	}
}
