package org.darkstorm.runescape.osbot.wrapper;

import java.awt.Point;

import org.darkstorm.runescape.api.Game;
import org.darkstorm.runescape.api.input.*;
import org.darkstorm.runescape.api.util.Tile;
import org.darkstorm.runescape.api.wrapper.Model;
import org.darkstorm.runescape.osbot.GameContextImpl;
import org.osbot.accessor.*;
import org.osbot.script.Wrapper;
import org.osbot.script.rs2.model.RS2Object;

public class GameObjectWrapper extends AbstractWrapper implements
		org.darkstorm.runescape.api.wrapper.GameObject {
	private final GameObjectType type;
	private final Tile tile;

	private XInteractableObject object;
	private XWallDecoration wallDecoration;
	private XWallObject boundary;
	private ModelWrapper model;
	private int id;

	public GameObjectWrapper(GameContextImpl context, XInteractableObject obj) {
		this(context, GameObjectType.INTERACTIVE, obj.getLocalX(), obj
				.getLocalY());
		id = (obj.getIdHash() >> 14) & 0x7FFF;
		object = obj;

		// XAnimable<?> animable = obj.getAnimable();
		// if(animable == null)
		// return;
		// Animable<?> wrapper = Wrapper.wrap(animable);
		// if(wrapper == null)
		// return;
		// org.osbot.script.rs2.model.Model wrappedModel = wrapper.getModel();
		// if(wrappedModel == null)
		// return;
		// model = new ModelWrapper(context, wrappedModel, this);
		RS2Object wrapper = (RS2Object) Wrapper.<Wrapper<?>> wrap(obj);
		if(wrapper == null)
			return;
		model = new ModelWrapper(context, wrapper.getModel(), this);
	}

	public GameObjectWrapper(GameContextImpl context, XWallDecoration dec) {
		this(context, GameObjectType.DECORATION, dec.getX(), dec.getY());
		id = (dec.getIdHash() >> 14) & 0x7FFF;
		wallDecoration = dec;

		// XAnimable<?> animable = dec.getAnimable1();
		// if(animable == null)
		// animable = dec.getAnimable2();
		// if(animable == null)
		// return;
		// Animable<?> wrapper = Wrapper.wrap(animable);
		// if(wrapper == null)
		// return;
		// org.osbot.script.rs2.model.Model wrappedModel = wrapper.getModel();
		// if(wrappedModel == null)
		// return;
		// model = new ModelWrapper(context, wrappedModel, this);
		RS2Object wrapper = (RS2Object) Wrapper.<Wrapper<?>> wrap(dec);
		if(wrapper == null)
			return;
		model = new ModelWrapper(context, wrapper.getModel(), this);
	}

	public GameObjectWrapper(GameContextImpl context, XWallObject boundary) {
		this(context, GameObjectType.BOUNDARY, boundary.getIdHash() & 0x7F,
				boundary.getIdHash() >> 7 & 0x7F);
		id = (boundary.getIdHash() >> 14) & 0x7FFF;
		this.boundary = boundary;

		// XAnimable<?> animable = boundary.getAnimable1();
		// if(animable == null)
		// animable = boundary.getAnimable2();
		// if(animable == null)
		// return;
		// Animable<?> wrapper = Wrapper.wrap(animable);
		// if(wrapper == null)
		// return;
		// org.osbot.script.rs2.model.Model wrappedModel = wrapper.getModel();
		// if(wrappedModel == null)
		// return;
		// model = new ModelWrapper(context, wrappedModel, this);
		RS2Object wrapper = (RS2Object) Wrapper.<Wrapper<?>> wrap(boundary);
		if(wrapper == null)
			return;
		model = new ModelWrapper(context, wrapper.getModel(), this);
	}

	private GameObjectWrapper(GameContextImpl context, GameObjectType type,
			int x, int y) {
		super(context);
		this.type = type;
		Game game = context.getGame();
		tile = new Tile(game.getRegionBaseX() + x, game.getRegionBaseY() + y,
				game.getCurrentFloor());
	}

	@Override
	public Model getModel() {
		return model;
	}

	@Override
	public GameObjectType getType() {
		return type;
	}

	@Override
	public Tile getLocation() {
		return tile;
	}

	@Override
	public int getId() {
		return id;
	}

	@Override
	public MouseTarget getTarget() {
		Model model = getModel();
		if(model == null)
			return null;
		return new ModelMouseTarget(model);
	}

	@Override
	public boolean isOnScreen() {
		return getModel().getTriangles().length > 0;
	}

	@Override
	public Point getScreenLocation() {
		Model model = getModel();
		if(model == null)
			return new Point(-1, -1);
		return model.getCenterPoint();
	}

	@Override
	public boolean equals(Object obj) {
		if(!(obj instanceof GameObjectWrapper && getClass() == obj.getClass()))
			return false;
		GameObjectWrapper o = (GameObjectWrapper) obj;
		if(object != null && o.object != null && object == o.object)
			return true;
		if(boundary != null && o.boundary != null && boundary == o.boundary)
			return true;
		if(wallDecoration != null && o.wallDecoration != null
				&& wallDecoration == o.wallDecoration)
			return true;
		return false;
	}
}
