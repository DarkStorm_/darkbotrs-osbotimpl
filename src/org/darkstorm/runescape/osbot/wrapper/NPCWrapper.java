package org.darkstorm.runescape.osbot.wrapper;

import org.darkstorm.runescape.api.wrapper.NPC;
import org.darkstorm.runescape.osbot.GameContextImpl;
import org.osbot.accessor.*;

public class NPCWrapper extends CharacterWrapper implements NPC {
	private XNPC handle;

	public NPCWrapper(GameContextImpl context, XNPC handle) {
		super(context, handle);
		this.handle = handle;
	}

	@Override
	public int getId() {
		XNPCDefinition definition = handle.getDefinition();
		if(definition == null)
			return -1;
		return definition.getId();
	}

	@Override
	public String getName() {
		XNPCDefinition definition = handle.getDefinition();
		if(definition == null)
			return null;
		return definition.getName();
	}

	@Override
	public boolean isCombatEntity() {
		return false;
	}

	@Override
	public String[] getActions() {
		return new String[0];
	}

	@Override
	public int getLevel() {
		XNPCDefinition definition = handle.getDefinition();
		if(definition == null)
			return -1;
		return definition.getCombatLevel();
	}
}
