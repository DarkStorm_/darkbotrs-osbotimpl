package org.darkstorm.runescape.osbot.wrapper;

import java.awt.*;

import org.darkstorm.runescape.api.input.*;
import org.darkstorm.runescape.api.util.Filter;
import org.darkstorm.runescape.api.wrapper.*;
import org.darkstorm.runescape.osbot.GameContextImpl;
import org.osbot.accessor.XRS2Interface;
import org.osbot.script.Wrapper;
import org.osbot.script.rs2.Client;
import org.osbot.script.rs2.ui.*;

public class InterfaceComponentWrapper extends AbstractWrapper implements
		InterfaceComponent {
	private Interface source;
	private InterfaceComponent parent;
	private int index;
	private XRS2Interface widget;
	private RS2InterfaceChild wrapped;

	public InterfaceComponentWrapper(GameContextImpl context, Interface source,
			int index, XRS2Interface widget) {
		this(context, source, null, index, widget);
	}

	public InterfaceComponentWrapper(GameContextImpl context, Interface source,
			InterfaceComponent parent, int index, XRS2Interface widget) {
		super(context);
		this.source = source;
		this.parent = parent;
		this.index = index;
		this.widget = widget;
		Client client = context.getBot().getClient();
		RS2Interface iface = client.getInterface(source.getId());
		wrapped = Wrapper.wrap(widget, new Object[] { iface });
	}

	@Override
	public int getId() {
		return index;
	}

	@Override
	public InterfaceComponent getComponent() {
		return this;
	}

	@Override
	public MouseTarget getTarget() {
		return new RectangleMouseTarget(getBounds());
	}

	@Override
	public InterfaceComponent[] getChildren() {
		return new InterfaceComponent[0];
		/*XRS2Interface[] widgets = widget.getChildren();
		if(widgets == null)
			return null;
		InterfaceComponent[] components = new InterfaceComponent[widgets.length];
		for(int i = 0; i < widgets.length; i++)
			if(widgets[i] != null)
				components[i] = new InterfaceComponentWrapper(context, source,
						this, i, widgets[i]);
		return components;*/
	}

	@Override
	public InterfaceComponent getChild(int id) {
		return null;
		/*IWidget[] widgets = widget.getChildren();
		if(widgets == null || widgets[id] == null)
			return null;
		return new InterfaceComponentWrapper(context, source, this, id,
				widgets[id]);*/
	}

	@Override
	public InterfaceComponent getChild(Filter<InterfaceComponent> filter) {
		return null;
	}

	@Override
	public int[] getItemIds() {
		return widget.getInv();
	}

	@Override
	public int[] getItemStackSizes() {
		return widget.getInvStackSizes();
	}

	@Override
	public int getContainedItemId() {
		return -1;
	}

	@Override
	public int getContainedItemStackSize() {
		return -1;
	}

	@Override
	public boolean hasParent() {
		return parent != null;
	}

	@Override
	public InterfaceComponent getParent() {
		return parent;
	}

	@Override
	public Interface getInterface() {
		return source;
	}

	@Override
	public Rectangle getRelativeBounds() {
		return new Rectangle(widget.getX(), widget.getY(), widget.getWidth(),
				widget.getHeight());
	}

	@Override
	public Rectangle getBounds() {
		Point pos = wrapped.getPosition();
		return new Rectangle(pos.x, pos.y, widget.getWidth(),
				widget.getHeight());
	}

	@Override
	public Point getCenter() {
		return new Point(widget.getX() + widget.getWidth() / 2, widget.getY()
				+ widget.getHeight() / 2);
	}

	@Override
	public String getText() {
		return widget.getMessage();
	}

	@Override
	public String getTooltip() {
		return widget.getToolTip();
	}

	@Override
	public String[] getActions() {
		return widget.getActions();
	}

	@Override
	public int getTextColor() {
		return -1;
	}

	@Override
	public boolean isInventory() {
		int[] slotContents = widget.getInv();
		return slotContents != null && slotContents.length > 0;
	}

	@Override
	public int getTextureId() {
		return widget.getSpriteIndex1();
	}

	@Override
	public int getScrollPosition() {
		return widget.getScrollPosition();
	}

	@Override
	public int getScrollHeight() {
		return widget.getScrollMax();
	}

	@Override
	public int getModelId() {
		return widget.getEnabledMediaId();
	}

	@Override
	public int getModelType() {
		return widget.getEnabledMediaType();
	}

	@Override
	public String getSelectedAction() {
		return widget.getSelectedActionName();
	}

	@Override
	public int getType() {
		return widget.getType();
	}

	@Override
	public boolean isValid() {
		return true;
	}

	@Override
	public boolean equals(Object obj) {
		if(!(obj instanceof InterfaceComponentWrapper && getClass() == obj
				.getClass()))
			return false;
		InterfaceComponentWrapper wrapper = (InterfaceComponentWrapper) obj;
		if(index != wrapper.index)
			return false;
		if(source.getId() != wrapper.source.getId())
			return false;
		if((parent == null || wrapper.parent == null)
				&& parent != wrapper.parent)
			return false;
		return parent == wrapper.parent || parent.equals(wrapper.parent);
	}
}
