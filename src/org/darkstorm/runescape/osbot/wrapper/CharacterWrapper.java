package org.darkstorm.runescape.osbot.wrapper;

import org.darkstorm.runescape.api.util.Tile;
import org.darkstorm.runescape.api.wrapper.Character;
import org.darkstorm.runescape.osbot.GameContextImpl;
import org.osbot.accessor.*;

public abstract class CharacterWrapper extends AnimableWrapper implements
		Character {
	private XCharacter<?> handle;

	public CharacterWrapper(GameContextImpl context, XCharacter<?> handle) {
		super(context, handle);
		this.handle = handle;
	}

	@Override
	public double getRotation() {
		return handle.getRotation();
	}

	@Override
	public Tile[] getWaypoints() {
		int[] waypointsX = handle.getWalkingQueueX().clone();
		int[] waypointsY = handle.getWalkingQueueY().clone();
		int plane = context.getClient().getPlane();
		if(waypointsX.length != waypointsY.length)
			return new Tile[0];
		Tile[] tiles = new Tile[waypointsX.length];
		for(int i = 0; i < tiles.length; i++)
			tiles[i] = new Tile(waypointsX[i], waypointsY[i], plane);
		return tiles;
	}

	@Override
	public String getOverheadMessage() {
		return null;
	}

	@Override
	public int getAnimation() {
		return handle.getAnimation();
	}

	@Override
	public boolean isInCombat() {
		return context.getClient().getCurrentTime() < handle.getCombatTime();
	}

	@Override
	public int getHealth() {
		return handle.getHealth();
	}

	@Override
	public int getMaxHealth() {
		return handle.getMaxHealth();
	}

	@Override
	public int getHealthPercentage() {
		return (int) (handle.getHealth() / (double) handle.getMaxHealth() * 100);
	}

	@Override
	public boolean isDead() {
		return handle.getHealth() <= 0;
	}

	@Override
	public int getMotion() {
		return -1;// handle.getWalkAnimation();
	}

	@Override
	public boolean isMoving() {
		return handle.getWalkingQueueSize() > 0;
	}

	@Override
	public Character getInteractionTarget() {
		int interacting = handle.getCharacterFacingUid();
		if(interacting == -1)
			return null;
		if(interacting < 32768) {
			XNPC[] npcs = context.getClient().getLocalNpcs();
			if(npcs == null || interacting >= npcs.length)
				return null;
			XNPC npc = npcs[interacting];
			if(npc == null)
				return null;
			return new NPCWrapper(context, npc);
		}
		XPlayer[] players = context.getClient().getLocalPlayers();
		if(players == null || interacting - 32768 >= players.length)
			return null;
		XPlayer player = players[interacting - 32768];
		if(player == null)
			return null;
		return new PlayerWrapper(context, player);
	}

	@Override
	public abstract int getLevel();

	@Override
	public Tile getLocation() {
		double x = context.getClient().getMapBaseX()
				+ (handle.getGridX() / 128D);
		double y = context.getClient().getMapBaseY()
				+ (handle.getGridY() / 128D);
		return new Tile(x, y, context.getClient().getPlane());
	}
}
