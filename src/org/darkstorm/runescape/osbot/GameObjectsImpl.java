package org.darkstorm.runescape.osbot;

import java.util.*;

import org.darkstorm.runescape.api.GameObjects;
import org.darkstorm.runescape.api.util.*;
import org.darkstorm.runescape.api.wrapper.GameObject;
import org.darkstorm.runescape.osbot.wrapper.GameObjectWrapper;
import org.osbot.accessor.*;

public final class GameObjectsImpl extends AbstractUtility implements
		GameObjects {

	public GameObjectsImpl(GameContextImpl context) {
		super(context);
	}

	@Override
	public GameObject getClosest(Filter<GameObject> filter) {
		GameObject closest = null;
		double minDist = 0;
		Tile self = context.getPlayers().getSelf().getLocation();
		for(GameObject gameObject : getAll()) {
			if(filter.accept(gameObject)) {
				double dist = self.distanceTo(gameObject.getLocation());
				if(dist < minDist || closest == null) {
					closest = gameObject;
					minDist = dist;
				}
			}
		}
		return closest;
	}

	@Override
	public GameObject[] getAll(Filter<GameObject> filter) {
		List<GameObject> accepted = new ArrayList<GameObject>();
		for(GameObject object : getAll())
			if(filter.accept(object))
				accepted.add(object);
		return accepted.toArray(new GameObject[accepted.size()]);
	}

	@Override
	public GameObject[] getAll() {
		XRegion sceneGraph = client.getCurrentRegion();
		if(sceneGraph == null)
			return new GameObject[0];
		int plane = client.getPlane();
		List<GameObject> objects = new ArrayList<GameObject>();
		XTile[][][] tiles = sceneGraph.getTiles();
		if(tiles.length <= plane)
			return new GameObject[0];
		XTile[][] planeTiles = tiles[plane];
		for(int x = 0; x < planeTiles.length; x++) {
			if(planeTiles[x] == null)
				continue;
			for(int y = 0; y < planeTiles[x].length; y++) {
				XTile tile = planeTiles[x][y];
				if(tile == null)
					continue;
				XWallObject wall = tile.getWallObject();
				XWallDecoration decoration = tile.getWallDecoration();
				XInteractableObject[] interactives = tile.getObjects();
				if(wall != null)
					objects.add(new GameObjectWrapper(context, wall));
				if(decoration != null)
					objects.add(new GameObjectWrapper(context, decoration));
				if(interactives != null)
					for(XInteractableObject interactive : interactives)
						if(interactive != null)
							objects.add(new GameObjectWrapper(context,
									interactive));
			}
		}
		for(GameObject object : objects.toArray(new GameObject[objects.size()]))
			if(object.getId() == -1)
				objects.remove(object);
		return objects.toArray(new GameObject[objects.size()]);
	}
}
