package org.darkstorm.runescape.osbot;

import org.darkstorm.runescape.api.*;
import org.darkstorm.runescape.api.input.*;
import org.osbot.accessor.XClient;

abstract class AbstractUtility implements Utility {
	protected final OSBot bot;
	protected final GameContextImpl context;
	protected final XClient client;

	protected final Calculations calculations;
	protected final Mouse mouse;
	protected final Keyboard keyboard;

	public AbstractUtility(GameContextImpl context) {
		bot = context.getBot();
		this.context = context;
		client = context.getClient();

		calculations = context.getCalculations();
		mouse = context.getMouse();
		keyboard = context.getKeyboard();
	}

	@Override
	public GameContext getContext() {
		return context;
	}
}
