package org.darkstorm.runescape.osbot;

import org.darkstorm.runescape.api.Settings;

public class SettingsImpl extends AbstractUtility implements Settings {

	public SettingsImpl(GameContextImpl context) {
		super(context);
	}

	@Override
	public int get(int setting) {
		return client.getConfigs1()[setting];
	}

	@Override
	public int getCount() {
		return client.getConfigs1().length;
	}
}
