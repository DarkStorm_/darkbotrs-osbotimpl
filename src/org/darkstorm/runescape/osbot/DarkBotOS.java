package org.darkstorm.runescape.osbot;

import java.awt.Frame;
import java.lang.reflect.*;

import javax.swing.JFrame;

import org.darkstorm.runescape.*;
import org.osbot.engine.Main;

public class DarkBotOS extends AbstractDarkBot {
	private final JFrame frame;
	private final org.osbot.script.Script script;

	public DarkBotOS(org.osbot.script.Script script) {
		this.script = script;
		frame = locateFrame();
	}

	private JFrame locateFrame() {
		for(Field field : Main.class.getDeclaredFields()) {
			Class<?> c = field.getType();
			if(!JFrame.class.isAssignableFrom(c)
					|| !Modifier.isStatic(field.getModifiers()))
				continue;
			field.setAccessible(true);
			try {
				return (JFrame) field.get(null);
			} catch(Exception e) {}
		}
		return null;
	}

	@Override
	public Bot createBot(GameType type) {
		if(type.equals(GameType.OLDSCHOOL))
			return new OSBot(this, script.getBot());
		return null;
	}

	@Override
	public Frame getFrame() {
		return frame;
	}
}
