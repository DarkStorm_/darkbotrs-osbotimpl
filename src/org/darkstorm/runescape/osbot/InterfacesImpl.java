package org.darkstorm.runescape.osbot;

import java.util.*;

import org.darkstorm.runescape.api.Interfaces;
import org.darkstorm.runescape.api.util.Filter;
import org.darkstorm.runescape.api.wrapper.*;
import org.darkstorm.runescape.osbot.wrapper.InterfaceWrapper;
import org.osbot.accessor.XRS2Interface;

public final class InterfacesImpl extends AbstractUtility implements Interfaces {

	public InterfacesImpl(GameContextImpl context) {
		super(context);
	}

	@Override
	public boolean interfaceComponentExists(int id, int childId) {
		return getComponent(id, childId) != null;
	}

	@Override
	public Interface getInterface(int id) {
		XRS2Interface[][] widgets = client.getInterfaces();
		if(id < 0 || id > widgets.length - 1 || widgets[id] == null)
			return null;
		return new InterfaceWrapper(context, id, widgets[id]);
	}

	@Override
	public InterfaceComponent getComponent(int id, int childId) {
		Interface i = getInterface(id);
		if(i == null)
			return null;
		return i.getComponent(childId);
	}

	@Override
	public InterfaceComponent getComponent(Filter<InterfaceComponent> filter) {
		XRS2Interface[][] widgets = client.getInterfaces();
		for(int i = 0; i < widgets.length; i++) {
			if(widgets[i] != null) {
				Interface iface = new InterfaceWrapper(context, i, widgets[i]);
				InterfaceComponent component = iface.getComponent(filter);
				if(component != null)
					return component;
			}
		}
		return null;
	}

	@Override
	public Interface getInterface(Filter<Interface> filter) {
		XRS2Interface[][] widgets = client.getInterfaces();
		for(int i = 0; i < widgets.length; i++) {
			if(widgets[i] != null) {
				Interface iface = new InterfaceWrapper(context, i, widgets[i]);
				if(filter.accept(iface))
					return iface;
			}
		}
		return null;
	}

	@Override
	public Interface[] getInterfaces(Filter<Interface> filter) {
		List<Interface> interfaces = new ArrayList<>();
		XRS2Interface[][] widgets = client.getInterfaces();
		for(int i = 0; i < widgets.length; i++) {
			if(widgets[i] != null) {
				Interface iface = new InterfaceWrapper(context, i, widgets[i]);
				if(filter.accept(iface))
					interfaces.add(iface);
			}
		}
		return interfaces.toArray(new Interface[interfaces.size()]);
	}

	@Override
	public Interface[] getInterfaces() {
		XRS2Interface[][] widgets = client.getInterfaces();
		Interface[] interfaces = new Interface[widgets.length];
		for(int i = 0; i < widgets.length; i++)
			if(widgets[i] != null)
				interfaces[i] = new InterfaceWrapper(context, i, widgets[i]);
		return interfaces;
	}

}
