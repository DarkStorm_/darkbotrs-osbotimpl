import java.awt.Graphics;
import java.util.logging.*;

import org.darkstorm.runescape.*;
import org.darkstorm.runescape.event.*;
import org.darkstorm.runescape.event.game.*;
import org.darkstorm.runescape.event.script.ScriptStopEvent;
import org.darkstorm.runescape.osbot.DarkBotOS;
import org.darkstorm.runescape.script.*;
import org.osbot.script.ScriptManifest;
import org.slf4j.LoggerFactory;

@ScriptManifest(name = "<name>", author = "<author>", info = "<description>", version = 1.337)
public class OSBotScript extends org.osbot.script.Script implements
		EventListener {
	private Script script;

	public OSBotScript() {
	}

	@Override
	public void onStart() {
		DarkBot darkbot = new DarkBotOS(this);
		Bot bot = darkbot.createBot(GameType.OLDSCHOOL);
		ScriptManager manager = bot.getScriptManager();
		try {
			Class<? extends Script> scriptClass = Class.forName("<script>")
					.asSubclass(Script.class);
			script = manager.loadScript(scriptClass);
			script.start();
			if(script instanceof AbstractScript) {
				final Logger logger = ((AbstractScript) script).getLogger();
				for(Handler handler : logger.getHandlers())
					logger.removeHandler(handler);
				final org.slf4j.Logger slfLogger = LoggerFactory
						.getLogger(logger.getName());
				logger.addHandler(new SLFLoggerProxy(slfLogger, logger));
			}
		} catch(ClassNotFoundException e) {
			e.printStackTrace();
		} catch(ScriptLoadException e) {
			e.printStackTrace();
		}
	}

	@Override
	public int onLoop() throws InterruptedException {
		if(script == null)
			return -1;
		return random(500, 1000);
	}

	@Override
	public void onPaint(Graphics g) {
		if(script == null)
			return;
		Bot bot = script.getBot();
		EventManager eventManager = bot.getEventManager();
		int width = bot.getGame().getWidth(), height = bot.getGame()
				.getHeight();
		eventManager.sendEvent(new PaintEvent(g, width, height));
	}

	@Override
	public void onMessage(String message) throws InterruptedException {
		if(script == null)
			return;
		Bot bot = script.getBot();
		EventManager eventManager = bot.getEventManager();
		eventManager.sendEvent(new ServerMessageEvent(message));
	}

	@Override
	public void onExit() {
		if(script != null && script.isActive())
			script.stop();
	}

	@EventHandler
	public void onScriptStop(ScriptStopEvent event) {
		if(script == null || script != event.getScript())
			return;
		try {
			stop();
		} catch(InterruptedException e) {}
	}
}
